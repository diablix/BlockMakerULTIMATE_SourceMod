public Plugin:myinfo = { name = "The BlockMaker: BunnyHop Block", author = "diablix", description = "", version = "0.1", url = "" }

#include <sourcemod>
#include <sdktools>

#include <blockmakerultimate_blocks>
#include <blockmakerultimate_inc>

//DYNAMIC PROPS W.I.P, for now:
#define BHOP_DISAPPEAR 	0.1
#define BHOP_REVERT		1.0

bool g_bTriggered[2048];

public void OnPluginStart(){
	BM_Register_BlockType(BLOCK_BHOP, "Bunnyhop", "models/playcore2020/normal/bunnyhop.mdl");
}

public void BM_OnBlockTouch(int iBlock, int iClient, blockTypes BlockType){
	if(IsBlockType(BlockType, BLOCK_BHOP) && !g_bTriggered[iBlock]){
		DataPack hData;
		(hData = CreateDataPack()).WriteCell(iBlock); hData.WriteCell(0); hData.WriteFloat(BHOP_REVERT);
		CreateTimer(BHOP_DISAPPEAR, taskBhopSwitch, hData);
		g_bTriggered[iBlock]=true;
	} 
}

public Action taskBhopSwitch(Handle hTimer, DataPack hData){
	hData.Reset();
	int iBlock = hData.ReadCell();
	
	if(!IsValidEntity(iBlock)) return;
	if(hData.ReadCell()){
		SetEntProp(iBlock, Prop_Data, "m_CollisionGroup", 0);
		SetEntityRenderColor(iBlock, 255, 255, 255, 255);
	}
	else{
		SetEntProp(iBlock, Prop_Data, "m_CollisionGroup", 2);
		SetEntityRenderColor(iBlock, 255, 255, 255, 80);
	}
	if(hData.ReadFloat()==BHOP_REVERT){
		DataPack hDP;
		(hDP = CreateDataPack()).WriteCell(iBlock); hDP.WriteCell(1); hDP.WriteFloat(0.0);
		CreateTimer(BHOP_REVERT, taskBhopSwitch, hDP);
		return;
	}
	g_bTriggered[iBlock] = false;
}