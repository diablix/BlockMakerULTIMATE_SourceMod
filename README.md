# BlockMakerULTIMATE for [![N|Solid](https://i.imgur.com/pgPxCB3.png)](https://sourcemod.net/)
A perfected tool that allows you to put interactive, fun entities on any MAP.
Developed by CS 1.6 EasyBlocks gamemode enthusiasts.

#### Features
- 3rd party plugin(sub-plugin) support for custom block types;
- Each block type and it's effect represents a bit value - combine various objects and their effects in realtime for maximum creativity;
- Online list with custom objects coded by users for everyone;
- Custom properties - lots of options for each entity;
- GlowFX Animation system to make certain objects eye-catching;
- Will update this feature list very shortly..

#### How to add new block types (objects)
- go to `include/blockmakerultimate_blocks.inc`
- edit `enum blockTypes(<<=1)`, to add new one, simply put your new unique blocktype ID, for example `BLOCK_FIRE` and increase `BLOCK_MAX` by 1:

#### Before

```
enum blockTypes(<<=1){
	BLOCK_NONE = 0x00001,
	BLOCK_PLATFORM,
	BLOCK_BHOP,

	BLOCK_MAX = 3
};
```
#### After
```
enum blockTypes(<<=1){
	BLOCK_NONE = 0x00001,
	BLOCK_PLATFORM,
	BLOCK_BHOP,
        BLOCK_FIRE,

	BLOCK_MAX = 4
};
```
- This is it. Now you can code your own sub-plugin using same block type ID that you've just added to `blockmakerultimate_blocks.inc` in previous steps. 
Use `BM_Register_BlockType` OnPluginStart and `BM_OnBlockTouch` Forward for it's action on touch, as in representative block types that come by default:
```
public Plugin:myinfo = { name = "The BlockMaker: Fire Block", author = "", description = "", version = "", url = "" }
#include <sourcemod>
#include <blockmakerultimate_blocks>
#include <blockmakerultimate_inc>

public void OnPluginStart(){
	BM_Register_BlockType(BLOCK_FIRE, "Fire", "models/playcore2020/normal/fire.mdl");
}

public void BM_OnBlockTouch(int iBlock, int iClient, blockTypes BlockType){
	if(IsBlockType(BlockType, BLOCK_FIRE)){
		//what happens when someone touches the BLOCK_FIRE
	} 
}
```
- This solution requires basic understanding of SourceMod plugins coding & might be a subject to change when I come up with better idea.
The goal was to make it easy to convert different builds between servers and give block types ability to combine their effects into one entity.

