public Action _hookSay(int iClient, int iArgs){ //testing purposes only atm
	char sOutput[128];
	GetCmdArgString(sOutput, sizeof sOutput);
	StripQuotes(sOutput);

	if(StrEqual(sOutput, "!bhop")||StrEqual(sOutput, "!platform")){ //testing commands to create entities
		int iNewEntity;
		if((iNewEntity=CreateEntityByName("prop_physics_override"))!=-1){
			SDKHook(Block[iNewEntity].BlockCreate(iClient, iNewEntity, (StrEqual(sOutput, "!bhop")?BLOCK_BHOP:BLOCK_PLATFORM), BLOCK_NORMAL), SDKHook_Touch, _hook_TouchBlock);
		}
	}
	else if(StrEqual(sOutput, "!menu")){
		_menuMainCreate(iClient); //beta-testing menu
	}
	return Plugin_Continue;
}