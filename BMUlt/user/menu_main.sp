
_menuMainCreate(iClient){ //later all can be pre-defined in this case
	Handle hMenu = CreateMenu(_menuMain);
	SetMenuTitle(hMenu,  "BlockMakerULTIMATE");

	AddMenuItem(hMenu, "func_spawn", "Create");
	AddMenuItem(hMenu, "spacer", "", ITEMDRAW_SPACER);
	
	SetMenuPagination(hMenu, MENU_NO_PAGINATION);
	SetMenuExitButton(hMenu, true);
	DisplayMenu(hMenu, iClient, 0);
}

_menuMain(Handle:hMenu, MenuAction:mAction, iClient, iParam){
	if(mAction == MenuAction_Select){
		new String:sChosen[64];
		GetMenuItem(hMenu, iParam, sChosen, sizeof sChosen);
			
		if(StrEqual(sChosen, "func_spawn")){
			int iNewEntity;
			if((iNewEntity=CreateEntityByName("prop_physics_override"))!=-1){
				SDKHook(Block[iNewEntity].BlockCreate(iClient, iNewEntity, BLOCK_PLATFORM, BLOCK_NORMAL), SDKHook_Touch, _hook_TouchBlock);
			}
			_menuMainCreate(iClient);
		}
	}
}